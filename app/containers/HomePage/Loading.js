import React from 'react';
import PropTypes from 'prop-types';

const Loading = ({ isLoading, pastDelay, error }) => (
  <div>
    { isLoading && pastDelay ? <p>Loading...</p> : null }
    { !isLoading && error ? <p>Error...</p> : null }
  </div>
);

Loading.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  pastDelay: PropTypes.bool.isRequired,
  error: PropTypes.object,
};

export default Loading;
