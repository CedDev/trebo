import styled from 'styled-components';

const HomepageWrapper = styled.div`
  background: #DEDEDE;
  color: #202020;
  padding: 1px;
  margin: 0;
  min-height: 100vh;
  padding: 1rem 2rem;
`;

export default HomepageWrapper;
