import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import injectReducer from 'utils/injectReducer';

// import Cards from 'data/cards.db';
import HomepageWrapper from './HomepageWrapper';
import Card from '../Card';
import HomepageData from './data';

const {
  addCards,
  // removeCards,
  // clearCards
} = HomepageData.actions;

const Homepage = ({ projects }) => (
  <HomepageWrapper>
    {
      projects.map(({ id, name }) => (
        <Card key={id} name={name} desc={id} />
      ))
    }
  </HomepageWrapper>
);

Homepage.propTypes = {
  projects: PropTypes.object,
};

const mapStateToProps = (state) => ({
  cards: state.get('homepage').get('cards'),
  projects: state.get('root').get('projects'),
});

const mapDispatchToProps = (dispatch) => ({
  addCard: (name, desc, type = 'default', author = 0) => { dispatch(addCards(name, desc, type, author)); },
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'homepage', reducer: HomepageData.reducer });

const HomepageContainer = compose(withReducer, withConnect)(Homepage);

export default HomepageContainer;
