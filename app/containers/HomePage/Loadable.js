/**
 * Asynchronously loads the component for HomePage
 */
import Loadable from 'react-loadable';
import Projects from 'data/projects.db';
import Loading from './Loading';
import HomepageLoadingData from './loaderData';
import { store } from '../../store';

const { addProject, clearProjects } = HomepageLoadingData.actions;

export default Loadable({
  loader: () => new Promise((resolve, reject) => {
    Projects.allDocs({ include_docs: true }).then((result) => {
      store.dispatch(clearProjects());
      result.rows.forEach(({ doc }) => store.dispatch(addProject({ id: doc._id, name: doc.name }))); // eslint-disable-line no-underscore-dangle
      resolve(import('./index'));
    }).catch((error) => {
      reject(error);
    });
  }),
  loading: Loading,
});
