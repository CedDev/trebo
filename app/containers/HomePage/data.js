import { ReduxAction, ReduxHelper } from 'utils/ReduxHelper';

/*
  {
    card: {
      name: '',
      desc: '',
      author: '',
      etc...
    },
    cards: []
  }
*/

const r = new ReduxHelper('homepage');

r.addAction(new ReduxAction('set', 'name', '').setPropertyName(['card', 'name']));
r.addAction(new ReduxAction('set', 'desc', '').setPropertyName(['card', 'desc']));
r.addAction(new ReduxAction('set', 'type', '').setPropertyName(['card', 'type']));
r.addAction(new ReduxAction('set', 'author', 0).setPropertyName(['card', 'author']));

r.addAction(new ReduxAction('add', 'cards', []));
r.addAction(new ReduxAction('remove', 'cards'));
r.addAction(new ReduxAction('clear', 'cards'));

export default {
  actions: r.getActions(),
  reducer: r.getReducer(),
};
