import { ReduxHelper, ReduxAction } from 'utils/ReduxHelper';

const r = new ReduxHelper('homepageLoader');
r.addAction(new ReduxAction('add', 'project', []).setPropertyName('projects'));
r.addAction(new ReduxAction('clear', 'projects'));

export default {
  actions: r.getActions(),
  reducer: r.getReducer(),
};
