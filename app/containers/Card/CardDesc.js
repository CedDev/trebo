import styled from 'styled-components';

const CardDesc = styled.p`
  font-family: 'Roboto', sans-serif;
  font-size: 12px;
  line-height: 1.6;
  padding: 0;
  margin: 0 .5rem;
`;

export default CardDesc;
