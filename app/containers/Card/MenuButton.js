import styled from 'styled-components';

const CloseButton = styled.span`
  position: absolute;
  top: .1rem;
  right: 2rem;
  font-size: 12px;
  color: #AAA;
  cursor: pointer;
  &:hover {
    color: #181818;
  }
`;

export default CloseButton;
