import { ReduxHelper, ReduxAction } from 'utils/ReduxHelper';

const r = new ReduxHelper('card');
r.addAction(new ReduxAction('set', 'name', 'Name'));
r.addAction(new ReduxAction('set', 'desc', 'The description of the card.'));
r.addAction(new ReduxAction('set', 'type', 'default'));
r.addAction(new ReduxAction('set', 'author', 0));
r.addAction(new ReduxAction('add', 'comments', []));
r.addAction(new ReduxAction('remove', 'comments'));
r.addAction(new ReduxAction('add', 'tasks', []));
r.addAction(new ReduxAction('remove', 'tasks'));
r.addAction(new ReduxAction('add', 'users', []));
r.addAction(new ReduxAction('remove', 'users'));
r.addAction(new ReduxAction('add', 'medias', []));
r.addAction(new ReduxAction('remove', 'medias'));

export default {
  actions: r.getActions(),
  reducer: r.getReducer(),
};
