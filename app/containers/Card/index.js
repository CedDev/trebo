import React from 'react';
import PropTypes from 'prop-types';
import FaIcon from '@fortawesome/react-fontawesome';
import { faTimes, faEllipsisV } from '@fortawesome/fontawesome-free-solid';

import CardWrapper from './CardWrapper';
import CardName from './CardName';
import CardDesc from './CardDesc';
import CloseButton from './CloseButton';
import MenuButton from './MenuButton';

const Card = ({ name, desc, type }) => {
  let color = 'blue';
  switch (type) {
    case 'info':
    case 'message':
      color = 'blue';
      break;
    case 'warn':
    case 'warning':
      color = 'yellow';
      break;
    case 'success':
      color = 'green';
      break;
    case 'bug':
    case 'issue':
    case 'error':
      color = 'red';
      break;
    case 'default':
    default:
      color = '#F8F8F8';
  }

  return (
    <CardWrapper color={color}>
      { name === '' ? null : <CardName>{ name }</CardName> }
      { desc === '' ? null : <CardDesc>{ desc }</CardDesc> }
      <CloseButton>
        <FaIcon iconDefinition={faTimes} />
      </CloseButton>
      <MenuButton>
        <FaIcon iconDefinition={faEllipsisV} />
      </MenuButton>
    </CardWrapper>
  );
};

Card.propTypes = {
  name: PropTypes.string,
  desc: PropTypes.string,
  type: PropTypes.string,
  // author: PropTypes.number,
};

export default Card;
