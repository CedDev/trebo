import styled from 'styled-components';

const CardWrapper = styled.div`
  position: relative;
  width: 100%;
  padding: .5rem;
  max-width: 480px;
  background: #F8F8F8;
  border: 1px solid #DDD;
  border-top: 3px solid ${({ color = 'blue' }) => color};
  box-shadow: 0 1px 4px rgba( 0,0,0, 0.15 );
`;

export default CardWrapper;
