import styled from 'styled-components';

const CardName = styled.h2`
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  line-height: 2;
  padding: 0;
  margin: 0 .5rem;
`;

export default CardName;
