import MenuButton from './MenuButton';

const CloseButton = MenuButton.extend`
  right: .5rem;
`;

export default CloseButton;
