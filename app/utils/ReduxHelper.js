import { fromJS, List } from 'immutable';
import ExistsException from '../exceptions/ExistsException';

export class ReduxAction {
  /**
   * Create a new action.
   * @param {string} type The type of the action
   * @param {string} name The name of the action
   * @param {*} initialValue The initial value of the state for this property if it's not null.
   */
  constructor(type, name, initialValue = null) {
    this.type = type;
    this.name = name;
    this.initialValue = initialValue;
    this.propertyName = [name];
    this.handler = null;
    this.dispatcher = null;
  }
  setType(type) { this.type = type; return this; } /** @param {string} type The type of the action */
  setName(name) { this.name = name; return this; } /** @param {string} name The name of the action */
  setInitialValue(value) { this.initialValue = value; return this; } /** @param {*} value The initial value of the state */
  setPropertyName(prop) { this.propertyName = prop.unshift ? prop : [prop]; return this; } /** @param {string|string[]} prop The path to the property */
  setHandler(handler) { this.handler = handler; return this; }
  setDispatcher(dispatcher) { this.dispatcher = dispatcher; return this; }
}

export class ReduxHelper {

  /**
   * Create a new helper scoped with a namespace.
   * @param {string} namespace The namespace to use for the actions
   */
  constructor(namespace) {
    this.namespace = namespace;
    this.defaultState = {};
    this.actions = {};
    this.map = {};

    this.defaultHandlers = {
      set: (state, action) => state.setIn(action.prop, action.value),
      add: (state, action) => state.updateIn(action.prop, (prop) => prop.push(action.value)),
      remove: (state, action) => state.updateIn(action.prop, (prop) => prop.remove(action.index)),
      clear: (state, action) => state.setIn(action.prop, new List()),
    };

    this.defaultDispatchers = {
      set: (type, prop, value) => ({ type, prop, value }),
      add: (type, prop, value) => ({ type, prop, value }),
      remove: (type, prop, index) => ({ type, prop, index }),
      clear: (type, prop) => ({ type, prop }),
    };
  }

  /**
   * Converts a string from the camel case format to the upper case format.
   * @param {string} value The value (in camel case format) to convert to upper case format.
   */
  fromCamelToUpper(value) {
    let newValue = '';
    let i;
    for (i = 0; i < value.length; i += 1) {
      if (value[i] === value[i].toUpperCase()) newValue += '_';
      newValue += value[i].toUpperCase();
    }
    return newValue;
  }

  makeActionName(type, name) {
    return type === '' ? name : type + name[0].toUpperCase() + name.substr(1);
  }

  makeActionType(type, name) {
    return this.fromCamelToUpper(
      type === ''
      ? this.namespace + name[0].toUpperCase() + name.substr(1)
      : type + this.namespace[0].toUpperCase() + this.namespace.substr(1) + name[0].toUpperCase() + name.substr(1)
    );
  }

  /**
   * Add and register an action to this set of actions.
   * @param {ReduxAction} action The action to add.
   */
  addAction(action) {
    const { type, name, initialValue = null, propertyName, handler = null, dispatcher = null } = action;
    const actionName = this.makeActionName(type, name);
    const upperName = this.makeActionType(type, name);

    if (this.actionExists(actionName)) {
      throw new ExistsException(`Trying to create the action '${actionName}' but it already exists.`);
    }

    let actionDispatcher;
    if (dispatcher === null) {
      switch (type) {
        case 'add': actionDispatcher = (value) => this.defaultDispatchers.add(upperName, propertyName, value); break;
        case 'remove': actionDispatcher = (index) => this.defaultDispatchers.remove(upperName, propertyName, index); break;
        case 'clear': actionDispatcher = () => this.defaultDispatchers.clear(upperName, propertyName); break;
        case 'set':
        default: actionDispatcher = (value) => this.defaultDispatchers.set(upperName, propertyName, value);
      }
    } else actionDispatcher = (...args) => dispatcher(upperName, propertyName, ...args);

    const actionHandler = handler === null ? (this.defaultHandlers[type] || this.defaultDispatchers.set) : handler;

    if (initialValue !== null && !(propertyName in this.defaultState)) this.defaultState[propertyName] = initialValue;
    this.map[upperName] = actionName;
    this.actions[actionName] = {
      name: upperName,
      prop: propertyName,
      dispatcher: actionDispatcher,
      handler: actionHandler,
    };
  }

  actionExists(name) {
    return name in this.actions;
  }

  actionTypeExists(type) {
    return type in this.map;
  }

  getActions() {
    const actions = {};
    const actionNames = Object.keys(this.actions);
    actionNames.forEach((actionName) => { actions[actionName] = this.actions[actionName].dispatcher; });
    return actions;
  }

  getReducer() {
    return (state = fromJS(this.defaultState), action) => {
      if (this.actionTypeExists(action.type)) return this.actions[this.map[action.type]].handler(state, action);
      return state;
    };
  }
}
