import PouchDB from 'pouchdb-browser';
const Projects = new PouchDB('projects');

Projects.allDocs({ include_docs: true }).then((result) => {
  if (result.rows === 0) {
    Projects.post({ name: 'Test Project' });
  }
});

export default Projects;
