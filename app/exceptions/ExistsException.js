/**
 * This exception occurs when an identifier is already mapped to a value.
 * @param {string} msg The message
 */
export default function (msg) {
  return {
    name: 'ExistsException',
    message: msg,
  };
}
